## 100.1.3

- CSP Whitelist added

## 100.1.2

- Config path updated

## 100.1.1

- Fix review data persistor
- Do not use deprecated inheritance in controllers
- Module dependencies updated

## 100.1.0

- First stable release
